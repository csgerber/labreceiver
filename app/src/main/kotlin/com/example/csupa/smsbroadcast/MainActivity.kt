package com.example.csupa.smsbroadcast

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.csupa.smsbroadcast.SMSReceiver.MessageInteractionListener
import java.util.*

//code from git@github.com:SpicX/smsbroadcast.git
class MainActivity : Activity(), MessageInteractionListener {
    // private static MainActivity activity;
    private val smsList = ArrayList<String>()
    private var arrayAdapter: ArrayAdapter<String>? = null

    private lateinit var listView: ListView
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (ContextCompat.checkSelfPermission(baseContext, Manifest.permission.ACCESS_NETWORK_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.ACCESS_NETWORK_STATE), 456)
        }
        if (ContextCompat.checkSelfPermission(baseContext, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf(Manifest.permission.READ_SMS), 123)
        }
        listView = findViewById(R.id.lv_list)
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, smsList)
        listView.adapter = arrayAdapter
        listView.onItemClickListener = ItemClickLitener
        button = findViewById(R.id.button)
        button.setOnClickListener { readSMS() }
    }

    override fun notifyUpdated() {
        readSMS()
    }

    private fun readSMS() {
        val contentResolver = contentResolver
        val smsCursor = contentResolver.query(Uri.parse(INBOX), null, null, null, null)
        val senderIndex = smsCursor!!.getColumnIndex("address")
        val messageIndex = smsCursor.getColumnIndex("body")
        if (messageIndex < 0 || !smsCursor.moveToFirst()) return
        arrayAdapter!!.clear()
        do {
            val sender = smsCursor.getString(senderIndex)
            val message = smsCursor.getString(messageIndex)
            val formattedText = String.format(resources.getString(R.string.sms_message), sender, message)
            arrayAdapter!!.add(Html.fromHtml(formattedText).toString())
        } while (smsCursor.moveToNext())
    }

    private val ItemClickLitener = OnItemClickListener { adapterView, view, i, l ->
        try {
            Toast.makeText(applicationContext, R.string.new_sms, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    companion object {
        private const val TAG = "MainActivity"
        private const val INBOX = "content://sms/inbox"
    }
}