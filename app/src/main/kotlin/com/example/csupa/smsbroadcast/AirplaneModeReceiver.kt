package com.example.csupa.smsbroadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class AirplaneModeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action === Intent.ACTION_AIRPLANE_MODE_CHANGED) {
            Toast.makeText(context, "Airplace Mode Changed", Toast.LENGTH_SHORT).show()
            Log.d("AIRPLANE", "airplane mode changed here!")
        }
    }
}