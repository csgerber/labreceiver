package com.example.csupa.smsbroadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.telephony.SmsMessage
import android.util.Log
import android.widget.Toast

//code from git@github.com:SpicX/smsbroadcast.git
class SMSReceiver : BroadcastReceiver() {
    private var messageInteractionListener: MessageInteractionListener? = null
    override fun onReceive(context: Context, intent: Intent) {
        val bundle = intent.extras
        try {
            if (bundle != null) {
                val pdusObj = bundle["pdus"] as Array<Any>? ?: arrayOf()

                for (i in pdusObj.indices) {
                    val sms = SmsMessage.createFromPdu(pdusObj[i] as ByteArray)
                    val phoneNumber = sms.displayOriginatingAddress
                    val message = sms.displayMessageBody
                    val formattedText = String.format(context.resources.getString(R.string.sms_message), phoneNumber, message)
                    Toast.makeText(context, formattedText, Toast.LENGTH_LONG).show()
                    messageInteractionListener = context as MessageInteractionListener
                }
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        } finally {
            DelayTask().execute()
        }
    }

    interface MessageInteractionListener {
        fun notifyUpdated()
    }

    private inner class DelayTask : AsyncTask<Void?, Void?, Void?>() {
        protected override fun doInBackground(vararg voids: Void?): Void? {
            try {
                Thread.sleep(6000L)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(aVoid: Void?) {
            messageInteractionListener!!.notifyUpdated()
            super.onPostExecute(aVoid)
        }
    }

    companion object {
        private const val TAG = "SMSBroadcastReceiver"
    }
}